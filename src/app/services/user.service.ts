import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user';

@Injectable({ providedIn: 'root' })
export class UserService {
  dataChange: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  constructor(private http: HttpClient) { }

  getAll() {
    // return this.http.get<User[]>(`http://localhost:3000/v1/user`).subscribe(data => {
    //   this.dataChange.next(data);
    // }, (error: HttpErrorResponse) => {
    //   console.log(error.name + ' ' + error.message);
    // });
    return this.http.get(`http://localhost:3000/v1/user`).pipe();
  }

  register(user: User) {
    return this.http.post(`http://localhost:3000/v1/auth/register`, user);
  }

  updateUser(id: number, user: User) {
    return this.http.put(`http://localhost:3000/v1/users/${id}`, user);
  }

  delete(id: number) {
    return this.http.delete(`http://localhost:3000/v1/users/${id}`);
  }
}
