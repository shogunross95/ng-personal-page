import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { DataSource } from '@angular/cdk/collections';
import { map } from 'rxjs/operators';

import { UserService } from '../../services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  displayedColumns = ['id', 'email', 'firstName', 'lastName', 'phoneNumber', 'createdAt'];
  // exampleDatabase: DataService | null;
  // dataSource: ExampleDataSource | null;
  index: number;
  id: number;


  constructor(private userService: UserService, public httpClient: HttpClient, public dialog: MatDialog) {

  }

  ngOnInit(): void {
    this.userService.getAll().subscribe(data => {
      console.log(data);
    });
  }

}
