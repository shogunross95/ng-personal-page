export class User {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  createAt: string;
  token: string;
}
